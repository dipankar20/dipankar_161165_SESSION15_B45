<?php
require_once("../vendor/autoload.php");

use App\Student;
use App\Result;

$newStudent = new Student();
$newResult = new Result();

$newStudent->setStudentName($_POST["studentName"]);
$newStudent->setStudentID($_POST["studentID"]);

$newResult->setBanglaNumber($_POST["banglaNumber"]);
$newResult->setEnglishNumber($_POST["englishNumber"]);
$newResult->setMathNumber($_POST["mathNumber"]);
?>
<table border="1" align="center">
    <tr>
        <td>Student Name:</td>
        <td><?php echo $newStudent->getStudentName();?></td>
    </tr>
    <tr>
        <td>Student ID:</td>
        <td><?php echo $newStudent->getStudentID();?></td>
    </tr>
    <tr>
        <td>Student Bangla Number:</td>
        <td><?php echo $newResult->getBanglaNumber();?></td>
        <td><?php echo $newResult->getBanglaGrade();?></td>
    </tr>
    <tr>
        <td>Student English Number:</td>
        <td><?php echo $newResult->getEnglishNumber();?></td>
        <td><?php echo $newResult->getEnglishGrade();?></td>
    </tr>
    <tr>
        <td>Student Math Number:</td>
        <td><?php echo $newResult->getMathNumber();?></td>
    </tr>
</table>
