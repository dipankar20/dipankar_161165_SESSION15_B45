<?php

namespace App;


class Student
{
    private $studentID;
    private $studentName;

    public function setStudentID($studentID)
    {
        $this->studentID = $studentID;
    }

    public function getStudentID()
    {
        return $this->studentID;
    }

    public function setStudentName($studentName)
    {
        $this->studentName = $studentName;
    }

    public function getStudentName()
    {
        return $this->studentName;
    }
}