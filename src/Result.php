<?php

namespace App;


class Result
{
    private $banglaNumber;
    private $englishNumber;
    private $mathNumber;
    private $banglaGrade;
    private $englishGrade;
    private $mathGrade;

    public function setBanglaNumber($banglaNumber)
    {
        $this->banglaNumber = $banglaNumber;
    }

    public function getBanglaNumber()
    {
        return $this->banglaNumber;
    }

    public function setEnglishNumber($englishNumber)
    {
        $this->englishNumber = $englishNumber;
    }

    public function getEnglishNumber()
    {
        return $this->englishNumber;
    }

    public function setMathNumber($mathNumber)
    {
        $this->mathNumber = $mathNumber;
    }

    public function getMathNumber()
    {
        return $this->mathNumber;
    }

    public function setBanglaGrade()
    {
       if($this->banglaNumber>=80 & $this->banglaNumber<=100){
           echo "A+";
       } else if($this->banglaNumber>=75 & $this->banglaNumber<=79){
           echo "A";
       } else if($this->banglaNumber>=70 & $this->banglaNumber<=74){
           echo "A-";
       } else if($this->banglaNumber>=65 & $this->banglaNumber<=69){
           echo "B+";
       } else if($this->banglaNumber>=60 & $this->banglaNumber<=64){
           echo "B";
       } else if($this->banglaNumber>=55 & $this->banglaNumber<=59){
           echo "B-";
       } else if($this->banglaNumber>=50 & $this->banglaNumber<=54){
           echo "C+";
       } else if($this->banglaNumber>=45 & $this->banglaNumber<=49){
           echo "C";
       } else if($this->banglaNumber>=40 & $this->banglaNumber<=44){
           echo "C-";
       } else if($this->banglaNumber<=39){
           echo "F";
       } else{
           echo "Invalid";
       }
    }

    public function getBanglaGrade()
    {
       return $this->setBanglaGrade();
        //return $this->banglaGrade;
    }

    public function setEnglishGrade()
    {
        if($this->englishNumber>=80 & $this->englishNumber<=100){
            echo "A+";
        } else if($this->englishNumber>=75 & $this->englishNumber<=79){
            echo "A";
        } else if($this->englishNumber>=70 & $this->englishNumber<=74){
            echo "A-";
        } else if($this->englishNumber>=65 & $this->englishNumber<=69){
            echo "B+";
        } else if($this->englishNumber>=60 & $this->englishNumber<=64){
            echo "B";
        } else if($this->englishNumber>=55 & $this->englishNumber<=59){
            echo "B-";
        } else if($this->englishNumber>=50 & $this->englishNumber<=54){
            echo "C+";
        } else if($this->englishNumber>=45 & $this->englishNumber<=49){
            echo "C";
        } else if($this->englishNumber>=40 & $this->englishNumber<=44){
            echo "C-";
        } else if($this->banglaNumber<=39){
            echo "F";
        } else{
            echo "Invalid";
        }
    }

    public function getEnglishGrade()
    {
        $this->setEnglishGrade();
        return $this->englishGrade;
    }

    public function setMathGrade($mathGrade)
    {
        $this->mathGrade = $mathGrade;
    }

    public function getMathGrade()
    {
        return $this->mathGrade;
    }

}
?>