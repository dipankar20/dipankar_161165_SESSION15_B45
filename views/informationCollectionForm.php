<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Grading System Using OOP</title>
</head>
<body>
<form action="process.php" method="post">
    <table border="1" align="center">
        <tr>
            <td>Enter Student Name</td>
            <td><input type="text" name="studentName" placeholder="Student Name"></td>
        </tr>
        <tr>
            <td>Enter Student ID</td>
            <td><input type="number" name="studentID" placeholder="Student ID"></td>
        </tr>
    </table><br/><br/>
    <table border="1" align="center">
        <tr>
            <td>Enter Bangla Number</td>
            <td><input type="number" name="banglaNumber" placeholder="Bangla Number"></td>
        </tr>
        <tr>
            <td>Enter English Number</td>
            <td><input type="number" name="englishNumber" placeholder="English Number"></td>
        </tr>
        <tr>
            <td>Enter Mathmatics Number</td>
            <td><input type="number" name="mathNumber" placeholder="Math Number"></td>
        </tr>
        <tr>
            <td colspan="2" align="right"><input type="submit" name="submit" value="Get Result"></td>
        </tr>
    </table>
</form>
</body>
</html>